#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
#include <time.h>
#include Header.h



void DrawMenu(std::vector<std::string> list);
int** CreateMatrix(short);
void PrintMatrix(int**, short);
void PrintDifferenceBetweenSumOfNumsOfMaxAndMinValueAtRow(int**, short);
bool IsMatrixSymmetric(int**, short);
int FindFirstPositiveValueUnderDiagonal(int**, short);


using namespace std;


int main()
{
srand(time(0));
setlocale(0, ru);
cout << Введите размер квадратной матрицы: ;
unsigned short matrixSize;
cin >> matrixSize;
system(cls);
vector<string> menu = { Создать матрицу , Просмотреть матрицу, Найти разность суммы цифр максимального и минимального элементов каждой строки, Проверить симметричность матрицы, Найти первый положительный элемент под побочной диагональю матрицы, Выход };
unsigned short item;
int** matrix = NULL;
do
{
DrawMenu(menu);
cin >> item;
system(cls);
switch (item)
{
case 1:
{
matrix = CreateMatrix(matrixSize);
}; break;
case 2:
{
PrintMatrix(matrix, matrixSize);
}; break;
case 3:
{
PrintDifferenceBetweenSumOfNumsOfMaxAndMinValueAtRow(matrix, matrixSize);
}; break;
case 4:
{
bool result = IsMatrixSymmetric(matrix, matrixSize);
cout << ((result) ? (Матрица симметрична относительно диагонали) : (Матрица не симметрична относительно диагонали));
}; break;
case 5:
{
cout << FindFirstPositiveValueUnderDiagonal(matrix, matrixSize);
}; break;
case 6:
{
exit(0);
}; break;
}
cout << endl << Нажмите любую клавишу, чтобы вернуться в меню...;
char exitSymbol = _getch();
system(cls);
} while (item != menu.size());
return 0;
}

