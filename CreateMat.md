#ifndef header_H
 #endif header_H
#define header_H
#include <iomanip>
#include <vector>
using namespace std;
const int MAX_RANDOM_VALUE = 10;
const int MIN_RANDOM_VALUE = -10;

int** CreateMatrix(short size)
{
int** resultMatrix = new int* [size];
for (int i = 0; i < size; i++)
{
resultMatrix[i] = new int[size];
}
for (short i = 0; i < size; i++)
{
for (short j = 0; j < size; j++)
{
resultMatrix[i][j] = rand() % (MAX_RANDOM_VALUE - MIN_RANDOM_VALUE + 1) + MIN_RANDOM_VALUE;
}
}

cout << Матрица создана.;
return resultMatrix;
}


